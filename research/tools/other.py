# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.join(__file__, "../../research"))
import argparse
import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import SubsetRandomSampler
from visdom import Visdom
from research.unet.unet_model import UNet
from research.tools.data_loading import *


def get_default_visdom_env():
    """
    Create and return default environment from visdom
    Returns
    -------
    Visdom
           Visdom class object
    """
    default_port = 8097
    default_hostname = "http://localhost"
    parser = argparse.ArgumentParser(description='Demo arguments')
    parser.add_argument('-port', metavar='port', type=int, default=default_port,
                        help='port the visdom server is running on.')
    parser.add_argument('-server', metavar='server', type=str,
                        default=default_hostname,
                        help='Server address of the target to run the demo on.')
    flags = parser.parse_args()
    viz = Visdom(port=flags.port, server=flags.server)

    assert viz.check_connection(timeout_seconds=3), \
        'No connection could be formed quickly'
    return viz


def pair_of_dataloaders(split_ratio, path_to_train_folder,
                        transform,
                        batch_size=128, num_workers=4,
                        names=("train", "val")):
    """
    Take path to folder with data and percentage that defines,
    how much of them should be used as training data.
    Shuffle and split data. Returns random sampling
    DataLoader without replacement for training and validation sets
    and sizes of this sets.
    Parameters
    ----------
    transform:
        trasforms that applied to data during loading
    names: (string,string)
        Pair of names for DataLoaders
    split_ratio: float
        How much of data should be used as training data
    path_to_train_folder: string
        Valid path to training data.
    batch_size: int
        Size of batch for DataLoaders
    num_workers
        Number of threads for data prepossessing
    Returns
    -------
    Pair of dictionaries.
    First of them contains DataLoaders for validation and training sets.
    Second contains sizes of validation and training sets.
    """
    transformed_data_set = NewDataset(root_dir=path_to_train_folder,
                                      transforms=transform)
    shuffle_data_set = True
    random_seed = 42
    dataset_size = len(transformed_data_set)
    indices = list(range(dataset_size))
    valid_split_index = int(np.floor(split_ratio * dataset_size))
    if shuffle_data_set:
        np.random.seed(random_seed)
        np.random.shuffle(indices)
    train_indices = indices[:valid_split_index]
    val_indices = indices[valid_split_index:]
    # Creating PT data samplers and loaders:
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)
    dataset_sizes = {names[0]: valid_split_index,
                     names[1]: dataset_size - valid_split_index}

    train_loader = torch.utils.data.DataLoader(transformed_data_set,
                                               batch_size=batch_size,
                                               sampler=train_sampler,
                                               num_workers=num_workers)
    validation_loader = torch.utils.data.DataLoader(transformed_data_set,
                                                    batch_size=batch_size,
                                                    sampler=valid_sampler,
                                                    num_workers=num_workers)

    dataloaders = {names[0]: train_loader, names[1]: validation_loader}
    return dataloaders, dataset_sizes


def rgb_classif_model_and_criterion(device, weight, is_pretrained):
    """
    Create resnet18 and changes fully connected layer.
    Parameters
    ----------
    weight: torch vector
        weights for every class for loss function
    is_pretrained: bool
        Define is network`s weights are predefined or not
    device: torch.device
        Define CPU or GPU will be used for training
    Returns
    -------
    model architecture and criterion in tuple
    """
    model = torchvision.models.resnet18(pretrained=is_pretrained)
    num_ftrs = model.fc.in_features
    # model.fc = nn.Sequential(nn.Linear(num_ftrs, 110), nn.BatchNorm1d(110),
    #                          nn.ReLU(), nn.Linear(110, 24))
    model.fc = nn.Linear(num_ftrs, len(get_name_of_classes()))
    model = model.to(device)
    criterion = nn.CrossEntropyLoss(weight=weight)
    return model, criterion


def gray_classif_model_and_criterion(device, weight,
                                     is_pretrained):
    """
    Create resnet18 and changes fully connected layer.
    Parameters
    ----------
    weight: torch vector
        weights for every class for loss function
    is_pretrained: bool
        Define is network`s weights are predefined or not
    device: torch.device
        Define CPU or GPU will be used for training
    Returns
    -------
    model architecture and criterion in tuple
    """
    model = torchvision.models.resnet18(pretrained=is_pretrained)
    model.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                            bias=False)
    num_ftrs = model.fc.in_features
    # model.fc = nn.Sequential(nn.Linear(num_ftrs, 110), nn.BatchNorm1d(110),
    #                          nn.ReLU(), nn.Linear(110, 24))
    model.fc = nn.Linear(num_ftrs, len(get_name_of_classes()))
    model = model.to(device)
    criterion = nn.CrossEntropyLoss(weight=weight)
    return model, criterion


def get_segment_model_and_criterion(device):
    """
    Create U-NET and changes fully connected layer.
    Parameters
    ----------
    device: torch.device
        Define CPU or GPU will be used for training
    Returns
    -------
    model architecture and criterion in tuple
    """
    model = UNet(n_channels=3, n_classes=1)
    model = model.to(device)
    criterion = nn.BCELoss()
    return model, criterion


def get_optimizer_and_scheduler(parameters, lr=0.3, momentum=0.9,
                                weight_decay=0.005, step_size=1, gamma=0.8):
    """
    Create and return SGD optimizer with arguments
    Parameters
    ----------
    parameters: weight of model that should be modified
    lr: float
        learning rate
    momentum: float
        momentum for SGD optimization
    weight_decay: float
        L2 regularization coefficient
    step_size: int
        define number steps after that learning rate will be multiplied by gamma
    gamma: float
        coefficient for learning rate decreasing
    Returns
    -------
    optimizer and scheduler in tuple
    """
    optimizer = optim.SGD(parameters, lr, momentum, weight_decay=weight_decay)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=gamma)
    return optimizer, scheduler


def get_segmented_image(image, prediction):
    """
    Apply segmentation on image using prediction map`s value
    Trashold 0.5
    Parameters
    ----------
    image: numpy array
        image to which segmentation are applied
    prediction
        map that shows confidence for corresponding pixel of image
    Returns
    -------
        image with applied segmentation
    """
    idx = np.argmax(prediction, 2)
    pred = np.max(prediction, 2)
    confidence_mask = pred > 0.3
    idx += 1
    idx *= confidence_mask
    return image * idx[:, :, np.newaxis]
