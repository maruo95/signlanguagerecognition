# -*- coding: utf-8 -*-
from __future__ import print_function, division
import sys
import os
sys.path.append(os.path.join(__file__, "../../research"))
import copy
import time
from tqdm import tqdm
from visdom import Visdom
from research.tools.transformers import *
from config import Config


def train_classifier(model, criterion, optimizer, scheduler, dataloaders,
                     device, viz, dataset_sizes, num_epochs=25):
    """
    Perform num_epochs steps of SGD
    Parameters
    ----------
    model: model of network
    criterion: loss function
    optimizer: algorithm of optimization
    scheduler: scheduler for learning rate decrease
    dataloaders: (torch.utils.data.DataLoader,torch.utils.data.DataLoader)
        pair of DataLoader for test and validation sets
    device: CPU or GPU
    viz: Visdom
        object created by Visdom constructor
    dataset_sizes: (int,int)
        pair of test set size and validation set size
    num_epochs
        amount of epochs
    Returns
    -------
        trained model
    """
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    acc_history = {'train': np.array([]), 'val': np.array([])}
    win = viz.line(
        X=np.column_stack((np.arange(0, 1), np.arange(0, 1))),
        Y=np.column_stack((np.arange(0, 1), np.arange(0, 1))),
    )
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            for data in tqdm(dataloaders[phase]):
                inputs = data['color'].float().to(device)
                labels = data['label'].to(device)

                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))
            # temp = epoch_acc.cpu().item()
            acc_history[phase] = np.append(acc_history[phase], epoch_acc.cpu())
            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())
        viz.line(
            X=np.column_stack(
                (np.arange(0, acc_history['train'].shape[0]),
                 np.arange(0, acc_history['val'].shape[0]))),
            Y=np.column_stack((acc_history['train'],
                               acc_history['val'])),
            win=win,
            update='insert',
            opts=dict(title='{}'.format(optimizer.defaults))
        )
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.
          format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model


def train_segmentator(model, criterion, optimizer, scheduler, dataloaders,
                      device, viz, dataset_sizes, num_epochs=25):
    """
    Perform num_epochs steps of SGD
    Parameters
    ----------
    model: model of network
    criterion: loss function
    optimizer: algorithm of optimization
    scheduler: scheduler for learning rate decrease
    dataloaders: (torch.utils.data.DataLoader,torch.utils.data.DataLoader)
        pair of DataLoader for test and validation sets
    device: CPU or GPU
    viz: Visdom
        object created by Visdom constructor
    dataset_sizes: (int,int)
        pair of test set size and validation set size
    num_epochs
        amount of epochs
    Returns
    -------
        trained model
    """
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    acc_history = {'train': np.array([]), 'val': np.array([])}
    win = viz.line(
        X=np.column_stack((np.arange(0, 1), np.arange(0, 1))),
        Y=np.column_stack((np.arange(0, 1), np.arange(0, 1))),
    )
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            for data in tqdm(dataloaders[phase]):
                inputs = data['color'].float().to(device)
                labels = data['mask'].to(device)

                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    pred, idx = torch.max(outputs, 1)
                    loss = criterion(outputs, labels.float())
                    confidence_mask = pred > 0.5
                    idx += 1
                    idx = idx * confidence_mask.long()
                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                corect_map, _ = torch.max(labels.data, 1)
                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects +=\
                    torch.sum(idx.float() == corect_map.float()) /\
                    torch.tensor(corect_map.shape[1] * corect_map.shape[2]).float()

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('{} Loss: {:.4f} Acc: {:.8f}'.format(
                phase, epoch_loss, epoch_acc))
            # temp = epoch_acc.cpu().item()
            acc_history[phase] = np.append(acc_history[phase], epoch_acc.cpu())
            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())
        viz.line(
            X=np.column_stack(
                (np.arange(0, acc_history['train'].shape[0]),
                 np.arange(0, acc_history['val'].shape[0]))),
            Y=np.column_stack((acc_history['train'],
                               acc_history['val'])),
            win=win,
            update='insert',
            opts=dict(title='{}'.format(optimizer.defaults))
        )
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.
          format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model


def resnet18(is_gray):
    """
    Create, train and save classification model.
    Returns
    -------

    """
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    with open('../config.cfg') as f:
        cfg = Config(f)
    train_data_path = cfg.training_data_path
    if is_gray:
        cur_transforms = gray_classif_training_transforms()
        cur_weights_path = cfg.classif_weights_path_gray
    else:
        cur_transforms = rgb_classif_training_transforms()
        cur_weights_path = cfg.classif_weights_path_rgb
    batch_size = 128
    train_split_ratio = 0.8
    is_pretrained = True
    # treshold_ratio_dsd = 0.25
    lr = 0.015
    momentum = 0.9
    weight_decay = 0.1
    step_size = 1
    gamma = 0.8
    num_epochs = 9
    viz = get_default_visdom_env()
    (dataloaders, dataset_sizes) = pair_of_dataloaders(train_split_ratio,
                                                       train_data_path,
                                                       cur_transforms,
                                                       batch_size, 12)

    weights_per_class = list(dataloaders['train'].dataset.samples_per_class.values())
    min_weight = np.max(weights_per_class)
    weights_per_class = torch.tensor(weights_per_class)
    weights_per_class = (1 / weights_per_class * min_weight).to(device)
    if is_gray:
        (model, criterion) = gray_classif_model_and_criterion(device,
                                                              weights_per_class,
                                                              is_pretrained)
    else:
        (model, criterion) = rgb_classif_model_and_criterion(device,
                                                             weights_per_class,
                                                             is_pretrained)

    model = model.to(device)
    (optimizer, scheduler) = get_optimizer_and_scheduler(model.parameters(),
                                                         lr, momentum,
                                                         weight_decay,
                                                         step_size, gamma)

    train_classifier(model, criterion, optimizer, scheduler, dataloaders,
                     device, viz, dataset_sizes, num_epochs)
    torch.save(model.state_dict(), cur_weights_path)


def unet():
    """
    Create, train and save segmentation model.
    Returns
    -------

    """
    with open('../config.cfg') as f:
        cfg = Config(f)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    batch_size = 12
    train_split_ratio = 0.8
    # treshold_ratio_dsd = 0.25
    lr = 0.03
    momentum = 0.9
    weight_decay = 0.05
    step_size = 1
    gamma = 0.1
    num_epochs = 11
    viz = get_default_visdom_env()

    (dataloaders, dataset_sizes) =\
        pair_of_dataloaders(train_split_ratio, cfg.training_data_path,
                            segment_training_transforms(),
                            batch_size=batch_size, num_workers=12)

    (model, criterion) = get_segment_model_and_criterion(device)

    model = model.to(device)

    (optimizer, scheduler) = get_optimizer_and_scheduler(model.parameters(),
                                                         lr, momentum,
                                                         weight_decay,
                                                         step_size, gamma)

    train_segmentator(model, criterion, optimizer, scheduler, dataloaders,
                      device, viz, dataset_sizes, num_epochs)
    torch.save(model.state_dict(), cfg.segment_weights_path)


if __name__ == '__main__':
    print(torch.cuda.is_available())
    resnet18(is_gray=False)
    #unet()
